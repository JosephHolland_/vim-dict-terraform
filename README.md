# Vim-Dict-Terraform #

### What is this repository for? ###

* Vim dictionary files with Terraform AWS provider resources and parameters

### How do I get set up? ###

#### Dependencies ####
 - vim
 - [vim-pathogen](https://github.com/tpope/vim-pathogen) - it just makes everything easier
 - [vim-terraform](https://github.com/hashivim/vim-terraform)

#### Deployment instructions ####

 1. Ensure vim and pathogen are installed correctly
 2. Install the vim-terraform plugin (it's excellent!)
 3. Change directory into the .vim folder, create a dicts folder and and download the terraform dictionaries

    ```
    cd ~/.vim
    mkdir dicts
    git clone https://bitbucket.org/JosephHolland_/vim-dict-terraform dicts/terraform
    ```

 4. Edit your .vimrc and add the following line to enable autocompletion from the dictionary files

    ```
    set complete+=k~/.vim/dict/*/*.dict
    ```

### Refreshing dictionaries ###

* Updating terraform aws resources and parameters from terraform source code.

    The really hacky way I went about getting the AWS provider resources and parameters was... awk!

 1. Clone terraform repo

    ```
    git clone https://github.com/hashicorp/terraform.git
    ```

 2. Run some bash command line magic

    Parameters:

    ```
    awk -F: /\&schema.Schema{/'{print $1}' builtin/providers/aws/* | sed 's/^[\t]*//;s/[ \t]*$//;s/"//g;s/s\[//g;s/\] =&schema.Schema{//g;s/resourceSchema\[//g' | grep -v -E 'schema.Schema|Elem' | sort | uniq  > ~/terraform-aws-parameters.dict
    ```

    Resources:

    ```
    awk -F: /\"aws_/'{print $1}' builtin/providers/aws/provider.go | sed 's/^[\t]*//;s/[ \t]*$//;s/"//g' | sort | uniq > ~/terraform-aws-resources.dict
    ```

### Contributing ###

 1. Fork it ( https://bitbucket.org/JosephHolland_/vim-dict-terraform/fork )
 2. Create your feature branch (git checkout -b my-new-feature)
 3. Commit your changes (git commit -am 'Add some feature')
 4. Push to the branch (git push origin my-new-feature)
 5. Create a new Pull Request

### Who do I talk to? ###

* Repo owner or admin